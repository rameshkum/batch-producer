package com.customer.kafka.batch;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.boot.context.properties.bind.BindException;

import com.customer.kafka.model.Employee;

import lombok.Data;

@Data
public class EmployeeFieldSetMapper  implements FieldSetMapper<Employee> {

	@Override
	public Employee mapFieldSet(FieldSet fieldSet) throws BindException {

		Employee emp = new Employee();

		emp.setId(fieldSet.readString("id"));
		emp.setFirstName(fieldSet.readString("firstName"));
		emp.setLastName(fieldSet.readString("lastName"));

		return emp;
	}

}