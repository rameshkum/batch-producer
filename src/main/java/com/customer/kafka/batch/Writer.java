package com.customer.kafka.batch;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import com.customer.kafka.model.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class Writer implements ItemWriter<Employee> {

    @Value("${app.topic.batch}")
    private String jsonTopic;
    
    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public void write(List<? extends Employee> employees) throws Exception {
    	employees.forEach(employee -> {
			try {
				
				Message<String> message = MessageBuilder
			                .withPayload(objectMapper.writeValueAsString(employee))
			                .setHeader(KafkaHeaders.TOPIC, jsonTopic)
			                .setHeader(KafkaHeaders.RECEIVED_MESSAGE_KEY, "999")
			                .setHeader("X-Custom-Header", "Sending Custom Header with Spring Kafka")
			                .build();
				kafkaTemplate.send(jsonTopic,  message.getPayload());
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
    }
}
