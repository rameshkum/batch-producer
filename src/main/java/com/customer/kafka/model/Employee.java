package com.customer.kafka.model;

import lombok.Data;

@Data
public class Employee {

	String id;
	String firstName;
	String lastName;

	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}

	public Employee(String id, String firstName, String lastName) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}
}
